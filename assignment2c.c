//
//  main.c
//  assignment2
//
//  Created by Razeen on 5/19/20.
//  Copyright © 2020 Razeen. All rights reserved.
//

#include <stdio.h>
int main() {
    char ch;
    int low, up;
    printf("Enter an Alphabet: ");
    scanf("%c", &ch);
    low= (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u');
    up = (ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U');
    if (low|| up)
        printf("%c is a vowel\n", ch);
    else
        printf("%c is a consonant\n", ch);
    return 0;
}